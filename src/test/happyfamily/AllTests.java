package happyfamily;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        HumanTest.class,
        SpeciesTest.class,
        PetTest.class,
        FamilyTest.class,
        FamilyServiceTest.class
})


public class AllTests {

}
