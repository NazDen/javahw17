package happyfamily;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class FamilyServiceTest {

    private FamilyDao familyDao=mock(CollectionFamilyDao.class);
    private FamilyService familyService= new FamilyService(familyDao);

    private FamilyService familyServiceNotMock= new FamilyService();




    @Test
    public void familyServiceGetAllFamiliesPositiveExecution() {
        //given
    List<Family> expectedList= new ArrayList<>();
    expectedList.add(new Family(new Women(),new Man()));
    expectedList.add(new Family(new Women(), new Man()));
    when(familyDao.getAllFamilies()).thenReturn(expectedList);
        //when
    List<Family> actualList= familyService.getAllFamilies();
        //then
    verify(familyDao, times(1)).getAllFamilies();
    assertEquals(expectedList,actualList);
    }

    @Test
    public void familyServiceDisplayAllFamiliesPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.add(new Family(new Women(), new Man()));
        when(familyDao.getAllFamilies()).thenReturn(expectedList);
        String expectedResult= "[Family{mother= Women{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, father= Man{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, children= [] pet= []}, Family{mother= Women{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, father= Man{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, children= [] pet= []}]";
        //when
        String actualResult=familyService.getAllFamilies().toString();
        //then
        assertEquals(expectedResult,actualResult);

    }

    @Test
    public void familyServiceGetFamiliesBiggerThanPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.add(new Family(new Women(), new Man()));
        when(familyDao.getAllFamilies()).thenReturn(expectedList);
        //when
        List<Family> actualList= familyService.getFamiliesBiggerThan(1);
        //then
        assertEquals(expectedList,actualList);

    }

    @Test
    public void familyServiceGetFamiliesLessThanPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.add(new Family(new Women(), new Man()));
        when(familyDao.getAllFamilies()).thenReturn(expectedList);
        //when
        List<Family> actualList= familyService.getFamiliesLessThan(3);
        //then
        assertEquals(expectedList,actualList);
    }

    @Test
    public void familyServiceCountFamiliesWithMemberNumberPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.add(new Family(new Women(), new Man()));
        when(familyDao.getAllFamilies()).thenReturn(expectedList);
        int expectedNumberOfFamilies= 2;
        //when
        int actualNumberOfFamilies= familyService.countFamiliesWithMemberNumber(2);
        //then
        assertEquals(expectedNumberOfFamilies,actualNumberOfFamilies);
    }

    @Test
    public void familyServiceCreateNewFamilyPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        Women mother= new Women();
        Man father= new Man();
        expectedList.add(new Family(mother,father));
        //when
        familyServiceNotMock.createNewFamily(mother,father);
        //then
        assertEquals(familyServiceNotMock.getAllFamilies(), expectedList);


    }

    @Test
    public void familyServiceDeleteFamilyByIndexPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        Women mother= new Women();
        Man father= new Man();
        expectedList.add(new Family(mother,father));
        expectedList.remove(0);
        //when
        familyServiceNotMock.createNewFamily(mother,father);
        familyServiceNotMock.deleteFamilyByIndex(0);
        //then
        assertEquals(familyServiceNotMock.getAllFamilies(),expectedList);
    }

    @Test
    public void familyServiceBornChildPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        Women mother= new Women();
        Man father= new Man();
        Family family = new Family(mother, father);
        mother.bornChild("","");
        expectedList.add(family);
        //when
        familyServiceNotMock.createNewFamily(mother,father);
        familyServiceNotMock.bornChild(familyServiceNotMock.getFamilyById(0),"","");
        //then
        assertEquals(familyServiceNotMock.getAllFamilies(),expectedList);
    }

    @Test
    public void familyServiceAdoptChildPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.get(0).addChild(new Man());
        //when
        familyServiceNotMock.createNewFamily(new Women(),new Man());
        familyServiceNotMock.adoptChild(familyServiceNotMock.getFamilyById(0),new Man());
        //then
        assertEquals(familyServiceNotMock.getFamilyById(0).getChildren(),expectedList.get(0).getChildren());
    }

    @Test
    public void familyServiceDeleteAllChildrenOlderThenPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.get(0).addChild(new Man());
        expectedList.get(0).getChildren().remove(0);
        //when
        familyServiceNotMock.createNewFamily(new Women(),new Man());
        familyServiceNotMock.bornChild(familyServiceNotMock.getFamilyById(0),"","");
        familyServiceNotMock.deleteAllChildrenOlderThen(-1);
        //then
        assertEquals(familyServiceNotMock.getFamilyById(0).getChildren(),expectedList.get(0).getChildren());
    }

    @Test
    public void familyServiceCountPositiveExecution() {
        //given
        int expectedNumber=1;
        //when
        familyServiceNotMock.createNewFamily(new Women(),new Man());

        //then
        assertEquals(familyServiceNotMock.count(),expectedNumber);
    }

    @Test
    public void familyServiceGetFamilyByIdPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        Women mother= new Women();
        Man father= new Man();
        Family family = new Family(mother, father);
        expectedList.add(family);

        //when
        familyServiceNotMock.createNewFamily(mother,father);

        //then
        assertEquals(familyServiceNotMock.getFamilyById(0),expectedList.get(0));

    }

    @Test
    public void familyServiceGetPetsPositiveExecution() {
        //given
        List<Family> expectedList= new ArrayList<>();
        expectedList.add(new Family(new Women(),new Man()));
        expectedList.get(0).getPet().add(new DomesticCat("",0,0,new HashSet<>()));
        //when
        familyServiceNotMock.createNewFamily(new Women(),new Man());
        familyServiceNotMock.addPet(0,new DomesticCat("",0,0,new HashSet<>()));
        //then
        assertEquals(familyServiceNotMock.getPets(0),expectedList.get(0).getPet());
    }

    @Test
    public void familyServiceAddPetPositiveExecution() {
        //given
        DomesticCat cat= new DomesticCat("",0,0,new HashSet<>());
        //when
        familyServiceNotMock.createNewFamily(new Women(),new Man());
        familyServiceNotMock.addPet(0,cat);
        //then
        assertTrue(familyServiceNotMock.getPets(0).contains(cat));

    }
}