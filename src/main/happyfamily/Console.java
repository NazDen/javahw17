package happyfamily;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Stream;

public class Console {

    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String REG_NUM = "([0-9])+";
    private static final String REG_STR = "\\D+";

    public void printCommand(){
        System.out.println("- 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)\n"+
                "- 2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)\n"+
                "- 3. Отобразить список семей, где количество людей больше заданного\n"+
                "- 4. Отобразить список семей, где количество людей меньше заданного\n"+
                "- 5. Подсчитать количество семей, где количество членов равно\n"+
                "- 6. Создать новую семью\n"+
                "- 7. Удалить семью по индексу семьи в общем списке\n"+
                "- 8. Редактировать семью по индексу семьи в общем списке\n"+
                "- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)\n");
//    - 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)
//- 2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)
//- 3. Отобразить список семей, где количество людей больше заданного
//  - запросить интересующее число
//- 4. Отобразить список семей, где количество людей меньше заданного
//  - запросить интересующее число
//- 5. Подсчитать количество семей, где количество членов равно
//  - запросить интересующее число
//- 6. Создать новую семью
//  - запросить имя матери
//  - запросить фамилию матери
//  - запросить год рождения матери
//  - запросить месяц рождения матери
//  - запросить день рождения матери
//  - запросить iq матери
//
//  - запросить имя отца
//  - запросить фамилию отца
//  - запросить год рождения отца
//  - запросить месяц рождения отца
//  - запросить день рождения отца
//  - запросить iq отца
//- 7. Удалить семью по индексу семьи в общем списке
//  - запросить порядковый номер семьи (ID)
//- 8. Редактировать семью по индексу семьи в общем списке
//  - 1. Родить ребенка
//    - запросить порядковый номер семьи (ID)
//    - запросить необходимые данные (какое имя дать мальчику, какое девочке)
//  - 2. Усыновить ребенка
//    - запросить порядковый номер семьи (ID)
//    - запросить необходимые данные (ФИО, год рождения, интеллект)
//  - 3. Вернуться в главное меню
//- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)
//  - запросить интересующий возраст
    }

    private String validEnter(String regex, String prntInfo){
        System.out.println(prntInfo);
        String string = SCANNER.next();
        while(!string.matches(regex)){
            System.out.println("\nWRONG DATA -> " + string);
            System.out.println(prntInfo);
            string = SCANNER.next();
        }
        return string;
    }

    public Women createWomen(){
        List<String>data=getData();
        Women women= null;
        try {
            women = new Women(data.get(0),data.get(1),data.get(4)+"/"+data.get(3)+"/"+data.get(2),Integer.parseInt(data.get(5)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return women;
    }

    private List getData() {
        List data=new ArrayList<>();
//        System.out.println("Введите имя:");
        data.add(validEnter("\\D+","Введите имя буквами:"));
//        System.out.println("Введите фамилию:");
        data.add(validEnter("\\D+","Введите фамилию буквам:"));
//        System.out.println("Введите год рождения:");
        data.add(validEnter("[0-9]{4}","Введите год рождения(4 цифри):"));
//        System.out.println("Введите месяц рождения цифрой:");
        data.add(validEnter("1[0-2]|0[1-9]", "Введите месяц цифрой от 1 до 12:"));
//        System.out.println("Введите день рождения:");
        data.add(validEnter("(1)[0-9]|(2)[0-9]|0[1-9]|3[0-1]", "Введите день от 1 до 31:"));
//        System.out.println("Введите iq:");
        data.add(validEnter("([0-9])+", "Введите уровень iq цифрой:"));
//        List<String> comands= Arrays.asList("Введите имя:","Введите фамилию:","Введите год рождения:","Введите месяц рождения цифрой:","Введите день рождения:","Введите iq:");
//        List<String> data=new ArrayList<>();
//        comands.forEach(s -> {System.out.println(s);
//            data.add(SCANNER.next());});
        return data;
    }

    public Man createMan(){
        List<String>data=getData();
        Man man= null;
        try {
            man = new Man(data.get(0),data.get(1),data.get(4)+"/"+data.get(3)+"/"+data.get(2),Integer.parseInt(data.get(5)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return man;
    }


public void consoleRun(){
    FamilyController familyController = new FamilyController();
    String command;
do {
    printCommand();
//    System.out.println("Введите номер команды: ");
    switch (command=validEnter("[1-9]|(exit)", "Введите номер команди от 1 до 9 или 'exit' для вихода:")){
        case "1":
            try {
                familyController.createNewFamily(new Women("Елена","Поп","14/02/1963", 68), new Man("Виктор","Поп","16/10/1955", 88));
                familyController.createNewFamily(new Women("Алла","Поп","11/05/1988",70),new Man("Николай","Бабочкин","01/02/1981",99));
                familyController.createNewFamily(new Women("Наташа","Дудкина","05/06/1968",80),new Man("Тарас", "Король", "11/02/1966",99));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            familyController.bornChild(familyController.getFamilyById(0), "Антон", "Вика");
            System.out.println("Семьи созданы.");
            break;
        case "2":
            familyController.displayAllFamilies();
            break;
        case "3":
//            System.out.println("Введите количество людей:");
            familyController.getFamiliesBiggerThan(Integer.parseInt(validEnter(REG_NUM, "Введите количество людей:")));
            break;
        case "4":
//            System.out.println("Введите количество людей:");
            familyController.getFamiliesBiggerThan(Integer.parseInt(validEnter(REG_NUM, "Введите количество людей:")));

            break;
        case "5":
            int i = familyController.countFamiliesWithMemberNumber(Integer.parseInt(validEnter(REG_NUM, "Введите количество людей:")));
            System.out.println("Количество семей:\t"+i);
            break;
        case "6":
            System.out.println("Введите данные матери");
            Women mother= createWomen();
            System.out.println("Введите данные отца");
            Man father= createMan();
            familyController.createNewFamily(mother,father);
            break;
        case "7":
            familyController.deleteFamilyByIndex(Integer.parseInt(validEnter(REG_NUM, "Введите порядковый номер семьи:"))-1);
            break;
        case "8":
            System.out.println("\t- 1. Родить ребенка\n"+
                               "\t- 2. Усыновить ребенка\n"+
                               "\t- 3. Вернуться в главное меню\n");
            int commandCase8= Integer.parseInt(validEnter("[1-3]", "Введите команду от 1 до 3:"));
            if (commandCase8== 1) {
//                System.out.println("Введите порядковый номер семьи:");
                int index= Integer.parseInt(validEnter(REG_NUM, "Введите порядковый номер семьи:"))-1;
//                System.out.println("Введите имя для девочки:");
                String girlName= validEnter(REG_STR, "Введите имя для девочки:");
//                System.out.println("Введите имя для мальчика:");
                String boyName= validEnter(REG_STR, "Введите имя для мальчика:");
                if (familyController.getFamilyById(index)!= null){
                    familyController.bornChild(familyController.getFamilyById(index),boyName,girlName);
                }else {
                    System.out.println("Семи с таким индексом не существуєт, введите корректный индекс или создайте семью");
                    System.out.println();
                }
                break;
            }else if(commandCase8== 2) {
//                System.out.println("Введите порядковый номер семьи:");
                int index= Integer.parseInt(validEnter(REG_NUM, "Введите порядковый номер семьи:"))-1;

//                System.out.println("Если хотите усыновить мальчика введите 1, если девочку введите 2:");
                int sex=Integer.parseInt(validEnter("[1-2]", "Если хотите усыновить мальчика введите 1, если девочку введите 2:"));

                    if (sex==1 && familyController.getFamilyById(index)!= null) {
                        Man boy = createMan();
                        familyController.adoptChild(familyController.getFamilyById(index),boy);
                    }else if (sex==2 && familyController.getFamilyById(index)!= null) {
                        Women girl = createWomen();
                        familyController.adoptChild(familyController.getFamilyById(index),girl);
                    }
                    else {
                        System.out.println("Семи с таким индексом не существуєт, введите корректный индекс или создайте семью");
                        System.out.println();
                    }
                    break;
            }else if (commandCase8== 3) {
                break;
            }
            break;
        case "9":
            int age= Integer.parseInt(validEnter(REG_NUM, "Введите возраст:"));
            familyController.deleteAllChildrenOlderThen(age);
            break;
        default:
            break;
    }
}while (!command.equals("exit"));

    }
}

