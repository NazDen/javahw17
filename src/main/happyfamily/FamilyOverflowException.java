package happyfamily;

public class FamilyOverflowException extends RuntimeException {

private int countFamily;

    public int getCountFamily() {
        return countFamily;
    }

    public FamilyOverflowException(String message, int countFamily) {
        super(message);
        this.countFamily = countFamily;
    }
}
